#
# Be sure to run `pod lib lint UI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UI'
  s.version          = '0.1.0'
  s.summary          = 'The UI dependencies for WhiteLabel.'

  s.description      = <<-DESC
  'The UI dependencies for WhiteLabel.'
                       DESC

  s.homepage         = 'https://gitlab.com/LucasBighi/ui'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lucas Bighi' => 'lucabig@itau-unibanco.com.br' }
  s.source           = { :git => 'https://gitlab.com/LucasBighi/ui.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'UI/Classes/**/*'
  s.swift_version = '5.0'

  # s.resource_bundles = {
  #   'UI' => ['UI/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'SteviaLayout'
end
