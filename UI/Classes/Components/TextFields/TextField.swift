//
//  TextField.swift
//  Pods-UI_Example
//
//  Created by Lucas Marques Bighi on 11/02/22.
//

import UIKit
import Stevia

public class TextField: UITextField {

    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()

    public override func draw(_ rect: CGRect) {
        sv(bottomLine)
        bottomLine.bottom(0).fillHorizontally().height(1)
        delegate = self
    }

    public init(text: String? = nil, placeholder: String? = nil) {
        super.init(frame: .zero)
        self.text = text
        self.placeholder = placeholder
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

    }
}

extension TextField: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomLine.backgroundColor = Theme.theme?.primaryColor
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        bottomLine.backgroundColor = .gray
    }
}

