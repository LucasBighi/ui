//
//  aaa.swift
//  Pods-UI_Example
//
//  Created by Lucas Marques Bighi on 10/02/22.
//

import UIKit
import Stevia

public extension UIFont {
    static func custom(ofSize size: CGFloat, weight: Weight) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: weight)
    }
}

public protocol ThemeProtocol {
    // MARK: View Colors
    var primaryColor: UIColor { get }
    var secondaryColor: UIColor { get }
    // MARK: Text Colors
    var primaryTextColor: UIColor { get }
    var secondaryTextColor: UIColor { get }

    // MARK: Fonts
    var headerFont: UIFont { get }
    var titleFont: UIFont { get }
    var subtitleFont: UIFont { get }

    // MARK: Common Button
    var buttonCornerRadius: CGFloat { get }
    // MARK: Primary Button
    func primaryButtonHeight(for state: Button.State) -> CGFloat
    func primaryButtonBackgroundColor(for state: Button.State) -> UIColor
    func primaryButtonBorderColor(for state: Button.State) -> UIColor
    func primaryButtonTitleColor(for state: Button.State) -> UIColor
    func primaryButtonFont(for state: Button.State) -> UIFont
    // MARK: Primary Button
    func secondaryButtonHeight(for state: Button.State) -> CGFloat
    func secondaryButtonBackgroundColor(for state: Button.State) -> UIColor
    func secondaryButtonBorderColor(for state: Button.State) -> UIColor
    func secondaryButtonTitleColor(for state: Button.State) -> UIColor
    func secondaryButtonFont(for state: Button.State) -> UIFont

    // MARK: ViewController
    var viewControllerBackgroundColor: UIColor { get }
}

public class Theme {
    static var theme: ThemeProtocol?

    public static func setTheme(_ theme: ThemeProtocol) {
        self.theme = theme
    }
}





public extension UILabel {

    enum Appearence {
        case header
        case title
        case subtitle
    }

    convenience init(text: String, appearence: Appearence) {
        self.init()
        self.text = text

        func labelStyle(_ l: UILabel) {
            switch appearence {
            case .header:
                l.font = Theme.theme?.headerFont
                l.textColor = Theme.theme?.primaryTextColor
            case .title:
                l.font = Theme.theme?.titleFont
                l.textColor = Theme.theme?.primaryTextColor
            case .subtitle:
                l.font = Theme.theme?.subtitleFont
                l.textColor = Theme.theme?.secondaryTextColor
            }
        }

        style(labelStyle)
    }
}

