//
    //  Button.swift
    //  Pods-UI_Example
    //
    //  Created by Lucas Marques Bighi on 11/02/22.
    //

    import UIKit

    fileprivate var actionKey: Void?

    public class Button: UIButton {

        public enum State {
            case enabled
            case disabled
        }

        var buttonState: State = .enabled

        private var _action: (() -> Void)? {
            get {
                return objc_getAssociatedObject(self, &actionKey) as? () -> Void
            }
            set {
                objc_setAssociatedObject(self, &actionKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }

        open override var intrinsicContentSize: CGSize {
            var size = super.intrinsicContentSize
            size.height = Theme.theme?.primaryButtonHeight(for: buttonState) ?? 0
            return size
        }

        public init(title: String, state: State, action: (() -> Void)?) {
            super.init(frame: .zero)
            commonInit(title: title, state: state, action: action)
        }

        public required init?(coder: NSCoder) {
            super.init(coder: coder)
            commonInit(title: title(for: .normal) ?? "", state: buttonState, action: nil)
        }

        func commonInit(title: String, state: State, action: (() -> Void)?) {
            self._action = action
            addTarget(self, action: #selector(pressed(sender:)), for: .touchUpInside)

            setTitle(title, for: .normal)

            func buttonStyle(_ b: UIButton) {
                layer.borderWidth = 5
                layer.cornerRadius = Theme.theme?.buttonCornerRadius ?? 0
            }

            style(buttonStyle)
        }

        @objc private func pressed(sender: UIButton) {
            _action?()
        }
    }

