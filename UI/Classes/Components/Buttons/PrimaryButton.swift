//
//  PrimaryButton.swift
//  Pods-UI_Example
//
//  Created by Lucas Marques Bighi on 11/02/22.
//

import UIKit
import Stevia

public class PrimaryButton: Button {

    public override init(title: String, state: Button.State, action: (() -> Void)?) {
        super.init(title: title, state: state, action: action)
        func buttonStyle(_ b: UIButton) {
            layer.borderWidth = 5
            layer.cornerRadius = Theme.theme?.buttonCornerRadius ?? 0
            b.backgroundColor = Theme.theme?.primaryButtonBackgroundColor(for: state)
            layer.borderColor = Theme.theme?.primaryButtonBorderColor(for: state).cgColor
            b.setTitleColor(Theme.theme?.primaryButtonTitleColor(for: state), for: .normal)
            b.titleLabel?.font = Theme.theme?.primaryButtonFont(for: state)
        }

        style(buttonStyle)
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit(title: title(for: .normal) ?? "", state: buttonState, action: nil)
    }
}
